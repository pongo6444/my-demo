<?php
/**
 * DEMO for NUOL Workshop
 * @author Krerk Piromsopa, Ph.D.
 */
require_once 'config.php';
require_once 'fileUtils.php';


if ($_FILES['document']) {
    $uploadfile = $DATA_DIR . basename($_FILES['document']['name']);
    if (move_uploaded_file($_FILES['document']['tmp_name'], $uploadfile)) {
        
    }
}

$list = listFiles($DATA_DIR);
?>
<html>
    <head>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet"> 
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <style type="text/css">
            body {
                display: flex;
                min-height: 100vh;
                flex-direction: column;
            }

            main {
                flex: 1 0 auto;
            }
        </style>
    </head>
    <body>
        <header>
            <nav>
                <div class="nav-wrapper">
                    <a href="#" class="brand-logo">Simple File Manager (Jing-Jing Demo)</a>                    
                </div>
            </nav>
        </header>
        <main class="container">            
            <h3>Directory</h3>
            <div class="row">                
                <?php
                foreach ($list as $f) {
                    ?>
                    <div class="col s4 center-align">
                        <a href="file-content.php?q=<?php echo $f ?>" target="_blank">
                            <i class="medium material-icons">insert_chart</i>
                            <br/>
                            <?php echo $f; ?>
                        </a>
                    </div>
                    <?php
                }
                ?>                
            </div>
            <div class="card">
                <form enctype="multipart/form-data"  method="POST">
                    <div class="card-content">
                        <span class="card-title">Upload File</span>
                        <!-- Name of input element determines name in $_FILES array -->
                        Select file: <input name="document" type="file" />
                    </div>
                    <div class="card-action">
                        <input type="submit" value="Upload" />
                    </div>
                </form>
            </div>
        </main>
        <footer class="page-footer">
            <div class="footer-copyright">                
                    By Krerk Piromsopa, Ph.D.                                   
            </div>
        </footer>
    </body>    
</html>
